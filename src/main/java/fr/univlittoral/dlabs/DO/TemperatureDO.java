package fr.univlittoral.dlabs.DO;

import javax.persistence.*;

@Entity
@Table(name="temperature")
public class TemperatureDO {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="temperature_id")
    private Integer id;
    @Column(name="temperature_value", nullable=false)
    private int value;
    @OneToOne(cascade={CascadeType.ALL})
    @JoinColumn(name="temperature", referencedColumnName = "user")
    @Column(name="fk_temperature_user")
    private UserDO creator;
    @ManyToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
    @JoinColumn(name="temperature", referencedColumnName = "deal")
    @Column(name="fk_temperature_deal")
    private DealDO deal;

    public TemperatureDO(Integer id, int value, UserDO creator, DealDO deal) {
        this.id = id;
        this.value = value;
        this.creator = creator;
        this.deal = deal;
    }

    public TemperatureDO() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public UserDO getCreator() {
        return creator;
    }

    public void setCreator(UserDO creator) {
        this.creator = creator;
    }

    public DealDO getDeal() {
        return deal;
    }

    public void setDeal(DealDO deal) {
        this.deal = deal;
    }
}
