package fr.univlittoral.dlabs.DO;

import javax.persistence.*;

@Entity
@Table(name="user")
public class UserDO {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="user_id")
    private Integer id;
    @Column(name="user_pseudo", nullable=false)
    private String pseudo;
    @Column(name="user_firstName")
    private String firstName;
    @Column(name="user_lastName")
    private String lastName;
    @Column(name="user_password")
    private String password;


    public UserDO(Integer id, String pseudo, String firstName, String lastName, String password) {
        this.id = id;
        this.pseudo = pseudo;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
    }

    public UserDO() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
