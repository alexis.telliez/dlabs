package fr.univlittoral.dlabs.DO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="deal")
public class DealDO {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="deal_id")
    private Integer id;
    @Column(name="deal_title", nullable=false)
    private String title;
    @Column(name="deal_shopName")
    private String shopName;
    @Column(name="deal_shopLink")
    private String shopLink;
    @Column(name="deal_priceOld")
    private double priceOld;
    @Column(name="deal_priceNew")
    private double priceNew;
    @Column(name="deal_promoCode")
    private String promoCode;
    @Column(name="deal_date")
    private Date date;
    @Column(name="deal_imgURL")
    private String imgURL;
    @Column(name="deal_description")
    private String description;
    @OneToOne(cascade={CascadeType.ALL})
    @JoinColumn(name="deal", referencedColumnName = "user")
    @Column(name="fk_deal_creator")
    private UserDO creator;

    public DealDO(Integer id, String title, String shopName, String shopLink, double priceOld, double priceNew, String promoCode, Date date, String imgURL, String description, UserDO creator) {
        this.id = id;
        this.title = title;
        this.shopName = shopName;
        this.shopLink = shopLink;
        this.priceOld = priceOld;
        this.priceNew = priceNew;
        this.promoCode = promoCode;
        this.date = date;
        this.imgURL = imgURL;
        this.description = description;
        this.creator = creator;
    }

    public DealDO() {}

    // Getters et setters

    public UserDO getCreator() {
        return creator;
    }

    public void setCreator(UserDO creator) {
        this.creator = creator;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopLink() {
        return shopLink;
    }

    public void setShopLink(String shopLink) {
        this.shopLink = shopLink;
    }

    public double getPriceOld() {
        return priceOld;
    }

    public void setPriceOld(double priceOld) {
        this.priceOld = priceOld;
    }

    public double getPriceNew() {
        return priceNew;
    }

    public void setPriceNew(double priceNew) {
        this.priceNew = priceNew;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}