package fr.univlittoral.dlabs.DTO;

import java.util.List;

public class DealsDTO {
    private List<DealDTO> deals;

    public DealsDTO(List<DealDTO> deals) {
        this.deals = deals;
    }

    public List<DealDTO> getDeals() {
        return this.deals;
    }

    public void setDeals(List<DealDTO> deals) {
        this.deals = deals;
    }


}