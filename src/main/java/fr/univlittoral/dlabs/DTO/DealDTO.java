package fr.univlittoral.dlabs.DTO;

import javax.persistence.Column;
import java.util.Date;

public class DealDTO {

    private Integer id;
    private String title;
    private String shopName;
    private String shopLink;
    private double priceOld;
    private double priceNew;
    private String promoCode;
    private int temperature;
    private String creator;
    private Date date;
    private String imgURL;
    private String description;

    public DealDTO(Integer id, String title, String shopName, String shopLink, double priceOld, double priceNew, String promoCode, int temperature, String creator, Date date, String imgURL, String description) {
        this.id = id;
        this.title = title;
        this.shopName = shopName;
        this.shopLink = shopLink;
        this.priceOld = priceOld;
        this.priceNew = priceNew;
        this.promoCode = promoCode;
        this.temperature = temperature;
        this.creator = creator;
        this.date = date;
        this.imgURL = imgURL;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopLink() {
        return shopLink;
    }

    public void setShopLink(String shopLink) {
        this.shopLink = shopLink;
    }

    public double getPriceOld() {
        return priceOld;
    }

    public void setPriceOld(double priceOld) {
        this.priceOld = priceOld;
    }

    public double getPriceNew() {
        return priceNew;
    }

    public void setPriceNew(double priceNew) {
        this.priceNew = priceNew;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
