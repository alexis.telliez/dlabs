package fr.univlittoral.dlabs.Services;

import fr.univlittoral.dlabs.DAO.ITemperatureRepository;
import fr.univlittoral.dlabs.DAO.IDealRepository;
import fr.univlittoral.dlabs.DAO.IUserRepository;
import fr.univlittoral.dlabs.DO.DealDO;
import fr.univlittoral.dlabs.DO.TemperatureDO;
import fr.univlittoral.dlabs.DO.UserDO;
import fr.univlittoral.dlabs.DTO.DealDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class DealService {


    @Autowired
    private IDealRepository dealRepository;
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private ITemperatureRepository temperatureRepository;


    public List<DealDTO> findAll() {

        final List<DealDO> dealService = dealRepository.findAll();
        final List<UserDO> userService = userRepository.findAll();
        final List<TemperatureDO> temperatureService = temperatureRepository.findAll();



        List<DealDTO> deals = new ArrayList<>();

        for(DealDO d : dealService)
        {
            deals.add(new DealDTO(
                    d.getId(),
                    d.getTitle(),
                    d.getShopName(),
                    d.getShopLink(),
                    d.getPriceOld(),
                    d.getPriceNew(),
                    d.getPromoCode(),
                    d.getTemperature(),
                    d.getCreator(),
                    d.getDate(),
                    d.getImgURL(),
                    d.getDescription()));
        }

        return deals;
    }

    public DealDTO findDeal(int id) {

        DealDO d = dealRepository.select(id);
        DealDTO deal = new DealDTO(
                d.getId(),
                d.getTitle(),
                d.getShopName(),
                d.getShopLink(),
                d.getPriceOld(),
                d.getPriceNew(),
                d.getPromoCode(),
                d.getTemperature(),
                d.getCreator(),
                d.getDate(),
                d.getImgURL(),
                d.getDescription());

        return deal;
    }

    public void addDeal(DealDTO deal){
        Date sqlDate = new Date(Calendar.getInstance().getTime().getTime());

        DealDO newDeal = new DealDO(
                deal.getId(),
                deal.getTitle(),
                deal.getShopName(),
                deal.getShopLink(),
                deal.getPriceOld(),
                deal.getPriceNew(),
                deal.getPromoCode(),
                deal.getTemperature(),
                "CREATOR",
                sqlDate,
                deal.getImgURL(),
                deal.getDescription());
        dealRepository.addDeal(newDeal);
    }
}
