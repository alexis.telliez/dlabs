package fr.univlittoral.dlabs.Controllers;

import fr.univlittoral.dlabs.DTO.DealDTO;
import fr.univlittoral.dlabs.DTO.DealsDTO;
import fr.univlittoral.dlabs.Services.DealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*" )
@RequestMapping(value = "/deals")
public class DealController {

    @Autowired
    private DealService dealService;


    @RequestMapping( method = RequestMethod.GET)
    public DealsDTO getDeals()
    {
        final List<DealDTO> deals = dealService.findAll();
        return new DealsDTO(deals);
    }

    @RequestMapping(value ="/{id}", method = RequestMethod.GET)
    public DealDTO getSingleDeal(@PathVariable int id){
        return dealService.findDeal(id);
    }

    @RequestMapping( method = RequestMethod.POST)
    public void createDeal(@RequestBody DealDTO deal) {
        dealService.addDeal(deal);
    }

}
